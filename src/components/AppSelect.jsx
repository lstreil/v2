import React, { useState, useEffect, useContext } from "react";
import { UserContext } from "./helpers/UserContext.jsx";

import { useTranslation } from "react-i18next";

function AppSelect(props) {
  const { t } = useTranslation();
  const [app, setApp] = useState("xxxx");
  const context = useContext(UserContext);
  const colors = [
    { name: "Administration", color: "#ffaaaa" },
    { name: "AUDIT", color: "#99dd99" },
    { name: "DSB", color: "#ddddff" },
    { name: "MRO", color: "#ddddaa" },
    { name: "PMO", color: "#aaddff" },
    { name: "NFM", color: "#aaddcc" },
    { name: "MMA", color: "#ffcc44" },
    { name: "TOOLBOX", color: "#ddaaff" }
  ];

  useEffect(() => {
    console.log("useEffect", context);
  });

  let apps = [];
  let appButtons = [];
  for (let i = 0; i < context.roles.length; i++) {
    let r = context.roles[i];
    let found = apps.find(e => {
      return e.name === r.app;
    });
    if (!found) {
      apps.push({ name: r.app });
    }
  }
  for (let i = 0; i < apps.length; i++) {
    let found = colors.find(e => {
      return e.name === apps[i].name;
    });
    let style = {
      display: "flex",
      height: "12rem",
      minWidth: "24rem",
      padding: "3rem",
      margin: "1rem",
      border: "1px solid black",
      backgroundColor: found.color
    };
    appButtons.push(
      <div key={apps[i].name} className="appButton" style={style}>
        {apps[i].name}
      </div>
    );
  }

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
        backgroundColor: "#f7f7f7"
      }}
    >
      {appButtons}
    </div>
  );
}

export { AppSelect };
