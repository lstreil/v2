import React from "react";
import { withRouter } from "react-router";
import { withTranslation } from "react-i18next";
import i18n from "i18next";
import moment from "moment";
import { UserContext } from "./../helpers/UserContext.jsx";
import { Button, Container, Form, Input } from "semantic-ui-react";
import { DrSaveButton } from "./../helpers/buttons/DrSaveButton.jsx";
import validator from "validator";

class ProfileImpl extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      display_name: "",
      email: "",
      email_new: "",
      language: "de",
      inRequest: false
    };
  }

  componentDidMount() {
    let user = JSON.parse(JSON.stringify(this.context.user));

    this.setState({
      name: user.name,
      display_name: user.display_name,
      email: user.email,
      email_new: user.email_new,
      language: user.language
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.inRequest) {
      this.setState({ inRequest: false });
    }
  }

  setLang = language => {
    i18n.changeLanguage(language, () => {
      moment.locale(language);
    });
    this.setState({ language: language });
  };

  changeEmail = () => {};

  onChange = (e, input) => {
    switch (input.id) {
      case "display_name":
        this.setState({ display_name: input.value });
        break;
      case "email_new":
        this.setState({ email_new: input.value });
        break;
      default:
    }
  };

  saveProfile = () => {
    this.setState(
      { changed: false },
      this.context.functions.saveProfile({
        display_name: this.state.display_name,
        email_new: this.state.email_new,
        language: this.state.language
      })
    );
  };

  render() {
    const { t } = this.props;

    if (this.state.name === "") {
      return null;
    }

    const validChars = /[a-z0-9. \- äöüßáàéèíìóòúùůýñňřšžâæçèéêëîïôœùûüÿ αβγδεζηθικλμνξοπρστυφχψω śźż]{3,20}/i;
    let display_name = this.state.display_name || "";
    let match = display_name.match(validChars);
    let nameOk = (match && match[0] === display_name) || display_name === "";

    let emailOK =
      validator.isEmail(this.state.email_new || "") || !this.state.email_new;

    let user = JSON.parse(JSON.stringify(this.context.user));
    let changed =
      user.display_name !== display_name ||
      user.email_new !== this.state.email_new ||
      user.language !== this.state.language;

    let enClass = "drFlag english";
    let deClass = "drFlag german";
    if (this.state.language === "en") {
      enClass += " active";
    }
    if (this.state.language === "de") {
      deClass += " active";
    }

    let roleOptions = [];
    for (let i = 0; i < this.context.roles.length; i++) {
      let r = this.context.roles[i];
      roleOptions.push({
        key: r.d_uuid,
        value: r.d_uuid,
        text: r.customerrole
      });
    }

    let roles = [];
    for (let i = 0; i < roleOptions.length; i++) {
      if (
        this.context.user.customerroles.find(e => {
          return e.id === roleOptions[i].value;
        })
      ) {
        let role = this.context.roles.find(e => {
          return e.d_uuid === roleOptions[i].value;
        });
        if (role) {
          roles.push({ role });
        }
      }
    }
    roles.sort((a, b) => {
      let aCmp = a.role.applicationname + a.role.customerrole;
      let bCmp = b.role.applicationname + b.role.customerrole;
      if (aCmp.toUpperCase() > bCmp.toUpperCase()) return 1;
      if (aCmp.toUpperCase() < bCmp.toUpperCase()) return -1;
      return 0;
    });

    let rolesDisplay = [];
    let app = "";
    let color;
    let colors = [
      { name: "Administration", color: "#ffaaaa" },
      { name: "AUDIT", color: "#99dd99" },
      { name: "DSB", color: "#ddddff" },
      { name: "MRO", color: "#ddddaa" },
      { name: "PMO", color: "#aaddff" },
      { name: "TOOLBOX", color: "#ddaaff" }
    ];
    for (let i = 0; i < roles.length; i++) {
      let r = roles[i].role;
      if (app !== r.applicationname) {
        if (app !== "") {
          rolesDisplay.push(
            <div
              key={"role_bottom" + i}
              style={{
                height: "8px",
                backgroundColor: color,
                borderBottom: "1px solid #aaaaaa"
              }}
            ></div>
          );
        }
        app = r.applicationname;
        color = colors.find(e => {
          return e.name === app;
        });
        if (color) {
          color = color.color;
        } else {
          color = "#dddddd";
        }
        rolesDisplay.push(
          <div
            key={"app_" + i}
            style={{
              borderTop: "1px solid #ffffff",
              paddingTop: "0.5rem",
              paddingLeft: "1rem",
              backgroundColor: color,
              fontWeight: "bold"
            }}
            title={r.info}
          >
            {r.applicationname}
          </div>
        );
      }
      rolesDisplay.push(
        <div
          key={"role_" + i}
          style={{
            paddingLeft: "3rem",
            backgroundColor: color
          }}
          title={r.comment}
        >
          {r.customerrole}
        </div>
      );
    }

    rolesDisplay.push(
      <div
        key={"role_end"}
        style={{
          height: "8px",
          backgroundColor: color,
          borderBottom: "1px solid #aaaaaa"
        }}
      ></div>
    );

    let formGroups = [];
    formGroups.push(
      <Form.Group key="name">
        <Form.Field inline width={4}>
          <label htmlFor={"name"}>{t("forms.profile.name")}</label>
          <Input disabled={true} id="name" value={this.state.name} />
        </Form.Field>
        <Form.Field inline width={4} error={!nameOk}>
          <label htmlFor={"display_name"}>
            {t("forms.profile.display_name")}
          </label>
          <Input
            id="display_name"
            value={display_name}
            placeholder={this.state.name}
            onChange={this.onChange}
          />
        </Form.Field>
      </Form.Group>
    );

    formGroups.push(
      <Form.Group key="email">
        <Form.Field inline width={4}>
          <label htmlFor={"email"}>{t("forms.profile.email")}</label>
          <Input disabled={true} id="email" value={this.state.email} />
        </Form.Field>
        <Form.Field inline width={4} error={!emailOK}>
          <label htmlFor={"email_new"}>{t("forms.profile.email_new")}</label>
          <Input
            id="email_new"
            value={this.state.email_new || ""}
            onChange={this.onChange}
          />
        </Form.Field>
      </Form.Group>
    );

    formGroups.push(
      <Form.Group key="lang">
        <Form.Field key={"language"} width={4}>
          <label>{t("forms.profile.language")}</label>
          <span className="DRlanguage">{t("languageLong")}</span>
          <Button className={enClass} onClick={() => this.setLang("en")} />
          <Button className={deClass} onClick={() => this.setLang("de")} />
        </Form.Field>
      </Form.Group>
    );

    formGroups.push(
      <Form.Group key="roles">
        <Form.Field
          inline
          width={16}
          className="view"
          style={{ backgroundColor: "transparent" }}
        >
          <label htmlFor={"roles"}>{t("forms.profile.roles")}</label>

          <div
            style={{
              width: "20rem",
              borderRight: "1px solid #aaaaaa",
              borderBottom: "1px solid #aaaaaa"
            }}
          >
            <div
              className="scrollTableWrapper"
              style={{
                maxHeight: "15rem",
                overflowY: "scroll"
              }}
            >
              {rolesDisplay}
            </div>
          </div>
        </Form.Field>
      </Form.Group>
    );

    formGroups.push(
      <Form.Group key="action">
        <Form.Field
          inline
          width={16}
          className="view"
          style={{ textAlign: "right" }}
        >
          <DrSaveButton
            disabled={!changed || !nameOk || !emailOK || this.state.inRequest}
            text={t("forms.profile.save")}
            onClick={this.saveProfile}
          />
        </Form.Field>
      </Form.Group>
    );

    let autocomplete = "new-password";
    //autocomplete = "on";
    return (
      <Container style={{ paddingTop: "2rem" }}>
        <h1>{t("forms.profile.title")}</h1>
        <Form autoComplete={autocomplete}>{formGroups}</Form>
      </Container>
    );
  }
}

ProfileImpl.contextType = UserContext;
const Profile1 = withTranslation()(ProfileImpl);
const Profile = withRouter(Profile1);
export { Profile };
