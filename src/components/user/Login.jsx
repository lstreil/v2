import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";

import { useTranslation } from "react-i18next";
import request from "./../helpers/request.js";
import { Button, Form } from "semantic-ui-react";
import { Password } from "./Password.jsx";
import { Message } from "./../helpers/Message.jsx";
import { Activate } from "./Activate.jsx";
import { PwReset } from "./PwReset.jsx";

function useQuery() {
  const { search } = useLocation();
  console.log("search", search);
  return React.useMemo(() => new URLSearchParams(search), [search]);
}

function Login(props) {
  let query = useQuery();
  let pKey = query.get("activationkey") || "";
  let pName = query.get("name") || "";

  const [activationkey] = useState(pKey);
  const { t } = useTranslation();
  const [name, setName] = useState(pName);
  const [password, setPassword] = useState("");
  const [formMessage] = useState("");

  useEffect(() => {
    console.log("useEffect", activationkey, name);
  });

  function validPassword(pw) {
    let rPw = /^[^:#?@]{8,20}$/;
    let match = pw.match(rPw);
    if (match && match[0] === pw) {
      return true;
    }
    return false;
  }

  function handleSubmit() {
    let tname = name.trim();
    let tpassword = password.trim();
    request(
      "POST",
      "usr/login",
      { name: tname, password: tpassword },
      response => props.setUser(response),
      error => console.log("request error during login", error)
    );
  }

  return (
    <div className="login" key={"LoginForm"}>
      <div className="applicationname">{t("application.name")}</div>
      {props.text && props.text.length > 0 ? (
        <Message text={props.text} className={"message warning"} />
      ) : (
        ""
      )}
      <Form className="login" autoComplete={"new password"}>
        <Form.Group>
          <Form.Field width={16}>
            <Form.Input
              id={"dr-account"}
              fluid
              label={t("forms.user.login.label.account")}
              placeholder=""
              tabIndex="1"
              value={name}
              onChange={(e, input) => setName(input.value)}
              required={true}
            />
          </Form.Field>
        </Form.Group>
        <Form.Group>
          <Form.Field width={16}>
            <Password
              id={"dr-password"}
              label={t("forms.user.login.label.password")}
              placeholder={""}
              tabIndex="2"
              value={password}
              onChange={(e, input) => setPassword(input.value)}
              onBlur={null}
            />
          </Form.Field>
        </Form.Group>
        <Form.Group>
          <Form.Field width={"sixteen"}>
            <Button
              fluid
              primary
              disabled={formMessage.length > 0 || !validPassword(password)}
              type="submit"
              tabIndex="2"
              onClick={handleSubmit}
            >
              {t("forms.user.login.button.login")}
            </Button>
            <hr />
            <PwReset />
            <Activate
              activationkey={activationkey}
              user={name}
              onOK={name => {
                this.setAccountName(name);
              }}
            />
          </Form.Field>
        </Form.Group>
      </Form>
      {formMessage.length > 0 ? (
        <Message text={formMessage} className={"message error"} />
      ) : (
        ""
      )}
      <div className="build">{props.build}</div>
    </div>
  );
}

export { Login };
