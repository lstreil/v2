import React from 'react';
import { withTranslation } from 'react-i18next';
import { Button } from 'semantic-ui-react';

class DrCancelButtonImpl extends React.Component {
  /*
   * decorator for save Button
   * props:
   *    onClick: onClick Function
   *    params: Parameter for onClick
   *    text: text in Button
   */
  render() {
    let className = 'cancelButton';
    if (this.props.extraClass) {
      className = className + ' ' + this.props.extraClass;
    }
    return (
      <Button
        className={className}
        onClick={e => {
          if (typeof this.props.params !== 'undefined') {
            this.props.onClick(this.props.params);
          } else {
            this.props.onClick();
          }
        }}
      >
        {this.props.text}
      </Button>
    );
  }
}
const DrCancelButton = withTranslation()(DrCancelButtonImpl);
export { DrCancelButton };
