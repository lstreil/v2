import React from 'react';
import { withTranslation } from 'react-i18next';
import { Popup, Button, Icon } from 'semantic-ui-react';
import { UserContext } from './../UserContext.jsx';

class DrSearchButtonImpl extends React.Component {
  /*
   * decorator for save Button
   * props:
   *    popup: Text in Popup
   *    disabled: disabled
   *    onClick: onClick Function
   *    params: Parameter for onClick
   *    text: text in Button
   */

  constructor(props) {
    super(props);
    this.state = {
      width: null,
    };
  }
  componentDidMount() {
    this.handleResize();
    window.addEventListener('resize', this.handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  handleResize = () => {
    let w = 'wide';
    if (window.innerWidth < 980) {
      w = 'small';
    }
    if (this.state.width !== w) {
      this.setState({ width: w });
    }
  };

  render() {
    let disabled = this.props.disabled;
    let className = 'searchButton';
    if (this.props.extraClass) {
      className += ' ' + this.props.extraClass;
    }

    if (disabled) {
      return (
        <Popup
          position="left center"
          disabled={!this.props.popup}
          offset={[0, 14]}
          style={{
            margin: '0',
            padding: '0',
            backgroundColor: '#6666ff',
            color: '#ffffff',
          }}
          hoverable
          content=<div className="popup">{this.props.popup}</div>
          trigger={
            <div style={{ display: 'inline' }}>
              <Button className={className} style={{ opacity: '0.45', cursor: 'not-allowed' }}>
                <Icon name={this.props.icon ? this.props.icon : 'search'} />
                {this.props.text}
              </Button>
            </div>
          }
        />
      );
    } else {
      if (this.state.width === 'wide') {
        return (
          <Button
            className={className}
            onClick={(e) => {
              if (typeof this.props.params !== 'undefined') {
                this.props.onClick(this.props.params);
              } else {
                this.props.onClick();
              }
            }}
          >
            <Icon name={this.props.icon ? this.props.icon : 'add'} />
            {this.props.text}
          </Button>
        );
      } else {
        return (
          <Popup
            position="top left"
            trigger={
              <Button
                className={className}
                onClick={(e) => {
                  if (typeof this.props.params !== 'undefined') {
                    this.props.onClick(this.props.params);
                  } else {
                    this.props.onClick();
                  }
                }}
              >
                <Icon name={this.props.icon ? this.props.icon : 'search'} />
              </Button>
            }
            content={this.props.text}
          />
        );
      }
    }
  }
}
DrSearchButtonImpl.contextType = UserContext;
const DrSearchButton = withTranslation()(DrSearchButtonImpl);
export { DrSearchButton };
