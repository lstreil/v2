import React from 'react';

class Expandable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      expanded: props.expanded || false
    };
  }

  toggle = (e, data) => {
    e.stopPropagation();
    this.setState({ expanded: !this.state.expanded });
  };

  ignoreClick = (e, data) => {
    e.stopPropagation();
  };

  render() {
    const { title, content } = this.props;
    let className = 'expandable closed';
    if (this.state.expanded) {
      className = 'expandable open';
    }

    return (
      <div className={className}>
        <div className="title" onClick={this.toggle}>
          {title}
        </div>
        <div className="content" onClick={this.ignoreClick}>
          {content}
        </div>
      </div>
    );
  }
}
//const Expandable = withTranslation()(ExpandableImpl);
export { Expandable };
