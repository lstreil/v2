import React from 'react';
import styled from 'styled-components';
import { Icon } from 'semantic-ui-react';

class DrToggleSize extends React.PureComponent {
  /*
   * decorator to toggle content (minimized / normal)
   * props:
   *    id: id/key of this element
   *    content: content to show when not minimized
   *    minContent: content to show when minimized
   *    minimized: if true, content is minimized
   *    pos: left|right - position of minimized content
   *    minimizeHandler: shown in content to minimize
   *    callback: function to let the caller know the minimized state
   */

  constructor(props) {
    let minStyle;
    if (props.pos === 'left') {
      minStyle = { left: '0px' };
    } else {
      minStyle = { right: '0px' };
    }

    const StyledTogglebutton = styled.div`
      margin: 0.1rem 0.25rem 0.1rem 0.25rem;
      padding: 0.1rem 0.25rem 0.1rem 0.25rem;
      cursor: pointer;
      position: absolute;
      transform: translate(0px, -1rem);
      background-color: transparent;
      z-index: 9999;
      border: 1px solid rgba(0, 0, 0, 0.25);
      &:hover {
        border: 1px solid rgba(0, 0, 0, 0.75);
        background-color: rgba(0, 0, 255, 0.25);
      }
    `;

    super(props);
    let minimizeHandler = props.minimizeHandler || <Icon name="triangle up" color="blue" style={{ zIndex: '9998' }} />;
    let minContent = props.minContent || <Icon name="triangle down" color="blue" style={{ zIndex: '9998' }} />;
    minContent = (
      <StyledTogglebutton style={minStyle} onClick={this.toggle}>
        {minContent}
      </StyledTogglebutton>
    );
    minimizeHandler = (
      <StyledTogglebutton style={minStyle} onClick={this.toggle}>
        {minimizeHandler}
      </StyledTogglebutton>
    );
    this.state = {
      id: props.id || 'toggleSize',
      minimized: props.minimized ? true : false,
      minimizeHandler: minimizeHandler,
      minContent: minContent
    };
  }

  toggle = () => {
    if (this.props.callback) {
      this.props.callback(!this.state.minimized);
    }
    this.setState({ minimized: !this.state.minimized });
  };

  render() {
    if (this.state.minimized) {
      return (
        <div key={this.state.id} id={this.state.id + '_minimized'}>
          {this.state.minContent}
        </div>
      );
    } else {
      return (
        <div key={this.state.id} id={this.state.id + '_normal'} style={{ position: 'relaitve' }}>
          {this.state.minimizeHandler}
          {this.props.content}
        </div>
      );
    }
  }
}
export { DrToggleSize };
