import React from 'react';
import { withTranslation } from 'react-i18next';
import { UserContext } from './../UserContext.jsx';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { Button } from 'semantic-ui-react';
//import { DrModal } from './DrModal.jsx';
import tableconfigDefaults from './../../../config/tableconfigDefaults.json';

const getItemStyle = (isDragging, draggableStyle, item, type, that) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: 'none',
  padding: '8px',
  margin: type === 'fields' ? '4px' : '0px',
  border: '1px solid #c0c0c0',
  width: type === 'fields' ? 'auto' : (item.width * 100) / 16 + '%',
  display: type === 'fields' ? 'inline-block' : 'inherit',
  flex: 'none',

  // change background colour if dragging
  background: isDragging ? '#ffffff' : '#f7f7f7',
  //position: 'relative',

  // styles we need to apply on draggables
  ...draggableStyle,
});

const getListStyle = (isDraggingOver) => ({
  background: 'transparent',
  border: '1px solid black',
  padding: '0',
  margin: '0',
  width: '100%',
  minHeight: '5rem',
  display: 'block',
  overflowX: 'auto',
});

const getHeaderListStyle = (isDraggingOver) => ({
  background: isDraggingOver ? '#ffffff' : '#d7d7d7',
  padding: '0',
  margin: '0',
  width: '100%',
  minHeight: '3rem',
  display: 'flex',
  overflowX: 'auto',
});

const clone = (obj) => {
  return JSON.parse(JSON.stringify(obj));
};

class TableconfigModalImpl extends React.Component {
  constructor(props) {
    super(props);

    this.faderRef = React.createRef();

    this.state = {
      changed: false,
      config: null,
      columns: [],
      fields: [],
      keeps: [],
      open: props.open ? props.open : false,
      faderStyle: { top: '0px', left: '0px' },
    };
  }

  componentWillUnmount() {}

  componentDidMount() {
    let tConfig = this.context.tableconfigs.find((e) => {
      return e.name === this.props.table;
    });
    if (!tConfig) {
      tConfig = tableconfigDefaults.find((e) => {
        return e.name === this.props.table;
      });
    }

    this.updateConfig(tConfig.cols, false);
  }

  updateConfig = (config, changed) => {
    let columns = [];
    let fields = [];
    let keeps = [];
    for (let i = 0; i < config.length; i++) {
      let entry = config[i];
      let text = entry.label;

      if (entry.translate) {
        text = this.props.t(entry.translate);
      } else {
        text = this.props.translations[entry.label] ? this.props.translations[entry.label] : entry.label;
      }
      if (entry.width > 0) {
        if (entry.show) {
          columns.push({ text: text, width: entry.width, entry: entry });
        } else {
          fields.push({ text: text, width: entry.width, entry: entry });
        }
      } else {
        keeps.push({ text: 'not shown', width: 0, entry: entry });
      }
    }

    this.setState({
      columns: columns,
      fields: fields,
      keeps: keeps,
      changed: changed,
    });
  };

  componentDidUpdate() {
    if (this.faderRef.current) {
      let rect = this.faderRef.current.getBoundingClientRect();
      let faderStyle = { top: -parseInt(rect.y) + 'px', left: -parseInt(rect.x) + 'px' };
      if (faderStyle.top !== '0px' && parseInt(this.state.faderStyle.top) !== parseInt(faderStyle.top)) {
        this.setState({ faderStyle: faderStyle });
      }
    }
    if (this.props.open !== this.state.open) {
      this.setState({ open: this.props.open });
    }
  }

  onDragEnd = (result) => {
    const { source, destination } = result;

    // dropped outside the list
    if (!destination) {
      return;
    }

    if (source.droppableId === 'menu' && destination.droppableId === 'menu') {
      let columns = clone(this.state.columns);
      let moved = columns.splice(source.index, 1);
      columns.splice(destination.index, 0, moved[0]);
      this.setState({ columns: columns, changed: true });
    }
    if (source.droppableId === 'fields' && destination.droppableId === 'fields') {
      let fields = clone(this.state.fields);
      let moved = fields.splice(source.index, 1);
      fields.splice(destination.index, 0, moved[0]);
      this.setState({ fields: fields });
    }
    if (source.droppableId === 'menu' && destination.droppableId === 'fields') {
      let columns = clone(this.state.columns);
      let fields = clone(this.state.fields);
      let moved = columns.splice(source.index, 1);
      fields.splice(destination.index, 0, moved[0]);
      this.setState({ columns: columns, fields: fields, changed: true });
    }
    if (source.droppableId === 'fields' && destination.droppableId === 'menu') {
      let columns = clone(this.state.columns);
      let fields = clone(this.state.fields);
      let moved = fields.splice(source.index, 1);
      columns.splice(destination.index, 0, moved[0]);
      this.setState({ columns: columns, fields: fields, changed: true });
    }
  };

  saveConfig = () => {
    let config = [];
    for (let i = 0; i < this.state.keeps.length; i++) {
      let entry = this.state.keeps[i].entry;
      config.push(entry);
    }
    for (let i = 0; i < this.state.columns.length; i++) {
      let entry = this.state.columns[i].entry;
      entry.show = true;
      entry.width = this.state.columns[i].width;
      config.push(entry);
    }
    for (let i = 0; i < this.state.fields.length; i++) {
      let entry = this.state.fields[i].entry;
      entry.show = false;
      entry.width = this.state.fields[i].width;
      config.push(entry);
    }
    this.props.onSubmit(config);
  };

  getAction = (col) => {
    let width = 0;
    for (let i = 0; i < this.state.columns.length; i++) {
      let w = this.state.columns[i].width;
      width += w;
      if (col === width - 1) {
        return { column: i, what: '+' };
      }
      if (col === width - 2) {
        return { column: i, what: '-' };
      }
    }
    return { column: -1, what: ' ' };
  };

  clickColumn = (action) => {
    if (action.column >= 0) {
      let columns = clone(this.state.columns);
      switch (action.what) {
        case '+':
          columns[action.column].width++;
          break;
        case '-':
          columns[action.column].width--;
          break;
        default:
      }
      this.setState({ columns: columns, changed: true });
    }
  };

  setDefault = () => {
    let defs = tableconfigDefaults.find((e) => {
      return e.name === this.props.table;
    });
    this.updateConfig(defs.cols, true);
  };

  render() {
    let cols = [];
    for (let i = 0; i < this.state.columns.length; i++) {
      let w = this.state.columns[i].width * 6.25 + '%';
      let style = {
        display: 'inline-block',
        position: 'relative',
        height: '2rem',
        maxHeight: '2rem',
        width: w,
        backgroundColor: '#f0f0f0',
        border: '1px solid #aaaaaa',
        fontSize: '120%',
        overflow: 'hidden',
      };
      let buttons = [];
      if (i > 0) {
        let action = { column: i - 1, what: '+' };
        buttons.push(
          <span
            onClick={(e) => {
              this.clickColumn(action);
            }}
            style={{
              cursor: 'pointer',
              position: 'absolute',
              left: '0px',
              top: '0.3rem',
              color: '#44bb00',
            }}
            key={'increase_' + i}
          >
            ▶
          </span>
        );
      }
      if (this.state.columns[i].width > 1) {
        let action = { column: i, what: '-' };
        buttons.push(
          <span
            onClick={(e) => {
              this.clickColumn(action);
            }}
            style={{
              cursor: 'pointer',
              position: 'absolute',
              right: '0px',
              top: '0.3rem',
              color: '#cc0000',
            }}
            key={'decrease_' + i}
          >
            ◀
          </span>
        );
      }
      cols.push(
        <div key={'col2_' + i} style={style}>
          {buttons}
        </div>
      );
    }
    let style = {
      display: 'inline-block',
      position: 'relative',
      height: '2rem',
      maxHeight: '2rem',
      width: '1rem',
      backgroundColor: '#f0f0f0',
      border: '1px solid #aaaaaa',
      fontSize: '120%',
      overflow: 'hidden',
    };
    cols.push(
      <div key={'colLast'} style={style}>
        <span
          onClick={(e) => {
            this.clickColumn({
              column: this.state.columns.length - 1,
              what: '+',
            });
          }}
          style={{
            cursor: 'pointer',
            position: 'absolute',
            left: '0px',
            top: '0.3rem',
            color: '#44bb00',
          }}
          key={'increase_last'}
        >
          ▶
        </span>
      </div>
    );

    let content = (
      <div style={{ backgroundColor: '#f0f0f0', padding: '2rem' }}>
        <h1 style={{ fontSize: '1.5rem', fontWeight: 'bold', margin: '1rem 0' }}>{this.props.t('tableConfig.title')}</h1>
        <p>{this.props.t('tableConfig.info')}</p>
        <div
          style={{
            textAlign: 'right',
            borderBottom: '1px solid #ffffff',
            backgroundColor: 'transparent',
          }}
        >
          <Button style={{ backgroundColor: '#88cc00' }} onClick={this.setDefault}>
            {this.props.t('tableConfig.default')}
          </Button>
        </div>
        <hr />
        <DragDropContext onDragEnd={this.onDragEnd}>
          <h1>{this.props.t('tableConfig.columns')}</h1>
          <div
            style={{
              padding: '0',
              margin: '1rem 0 0 0',
              width: '100%',
              height: '2rem',
              overflow: 'hidden',
            }}
          >
            {cols}
          </div>
          <Droppable droppableId="menu" direction="horizontal">
            {(provided, snapshot) => (
              <div ref={provided.innerRef} style={getHeaderListStyle(snapshot.isDraggingOver)}>
                {this.state.columns.map((item, index) => {
                  return (
                    <Draggable key={'menuItem_' + item.entry.col} draggableId={item.entry.col} index={index}>
                      {(provided, snapshot) => (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          style={getItemStyle(snapshot.isDragging, provided.draggableProps.style, item, 'menu', this)}
                        >
                          {item.text}
                        </div>
                      )}
                    </Draggable>
                  );
                })}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
          <hr />
          <h1>{this.props.t('tableConfig.fields')}</h1>
          <Droppable droppableId="fields">
            {(provided, snapshot) => (
              <div ref={provided.innerRef} style={getListStyle(snapshot.isDraggingOver)}>
                {this.state.fields.map((item, index) => (
                  <Draggable key={'fieldItem_' + item.entry.col} draggableId={item.entry.col} index={index}>
                    {(provided, snapshot) => (
                      <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        style={getItemStyle(snapshot.isDragging, provided.draggableProps.style, item, 'fields', this)}
                      >
                        {item.text}
                      </div>
                    )}
                  </Draggable>
                ))}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </DragDropContext>
        <div
          style={{
            padding: '2rem',
            textAlign: 'right',
            backgroundColor: 'transparent',
          }}
        >
          <Button disabled={!this.state.changed} primary onClick={this.saveConfig}>
            {this.props.t('tableConfig.save')}
          </Button>
        </div>
      </div>
    );
    //return <DrModal open={this.state.open} trigger={this.props.trigger} onClose={this.props.onClose} content={content} />;
    if (this.state.open) {
      let style = {
        position: 'absolute',
        top: this.state.faderStyle.top,
        left: this.state.faderStyle.left,
        width: '100vw',
        height: '100vh',
        backgroundColor: 'rgba(0,0,0,0.8)',
        zIndex: '12000',
      };

      return (
        <div
          id="fader"
          ref={this.faderRef}
          style={style}
          onClick={(e) => {
            if (e.target.id === 'fader') {
              this.props.onClose();
            }
          }}
        >
          <div
            id="workbench"
            style={{
              position: 'absolute',
              top: '10%',
              left: '5%',
              width: '90%',
            }}
          >
            {content}
          </div>
        </div>
      );
    } else {
      return this.props.trigger;
    }
    /*<Modal
        size="large"
        open={this.state.open}
        trigger={this.props.trigger}
        onClose={this.props.onClose}
        content={content}
      />*/
  }
}

TableconfigModalImpl.contextType = UserContext;
const TableconfigModal = withTranslation()(TableconfigModalImpl);
export { TableconfigModal };
