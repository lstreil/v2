import React from 'react';
import { Table, Icon } from 'semantic-ui-react';
import { UserContext } from './../UserContext.jsx';
import { TableconfigModal } from './TableconfigModal.jsx';
import { withTranslation } from 'react-i18next';
import tableconfigDefaults from './../../../config/tableconfigDefaults.json';
import moment from 'moment';

function extract(val, cfg, context, data) {
  let v = '';
  let src;
  let tmp = cfg.src.split('.');
  if (tmp.length > 1) {
    if (tmp[0] === 'context') {
      src = context[tmp[1]];
    } else {
      console.log('ERROR in tableconfig: invalid src (' + cfg.src + ')');
      return '';
    }
  } else {
    src = data[tmp[0]];
  }

  if (cfg.key) {
    let entry = src.find((e) => {
      if (cfg.keyVal) {
        return e[cfg.key] === val[cfg.keyVal];
      } else {
        return e[cfg.key] === val;
      }
    });
    if (entry) {
      v = [];
      for (let i = 0; i < cfg.vals.length; i++) {
        v.push(entry[cfg.vals[i]]);
      }
      v = v.join(cfg.innerSep ? cfg.innerSep : ' ');
    }
  } else {
    let vInner = [];
    for (let i = 0; i < src.length; i++) {
      for (let k = 0; k < cfg.vals.length; k++) {
        vInner.push(src[i][cfg.vals[k]]);
      }
    }
    v = vInner.join(cfg.innerSep ? cfg.innerSep : ' ');
  }
  return v;
}

class DrTableImpl extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      colCfg: null,
      sortBy: null,
      sortDir: null,
      loaded: this.props.loaded,
      table: this.props.table,
    };
  }

  componentDidMount() {
    let tConfig;
    if (this.props.config) {
      tConfig = this.props.config;
    } else {
      tConfig = this.context.tableconfigs.find((e) => {
        return e.name === this.props.table;
      });
    }

    if (!tConfig) {
      tConfig = tableconfigDefaults.find((e) => {
        return e.name === this.props.table;
      });
    }

    let sort = tConfig.sort;
    this.setState({
      sortBy: sort.col,
      sortDir: sort.dir,
      colCfg: tConfig.cols,
      table: this.props.table,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.loaded !== this.props.loaded) {
      this.setState({ loaded: this.props.loaded });
    }
    if (this.props.rerender || this.state.table !== this.props.table) {
      let tConfig;
      if (this.props.config) {
        tConfig = this.props.config;
      } else {
        tConfig = this.context.tableconfigs.find((e) => {
          return e.name === this.props.table;
        });
      }
      let sort = tConfig.sort;
      this.setState({
        sortBy: sort.col,
        sortDir: sort.dir,
        colCfg: tConfig.cols,
        table: this.props.table,
      });
    }
  }

  sort = (column) => {
    let sortBy = this.state.sortBy;
    let sortDir = this.state.sortDir;
    if (sortBy === column) {
      if (sortDir === 1) {
        sortDir = -1;
      } else {
        sortDir = 1;
      }
    } else {
      sortBy = column;
      sortDir = 1;
    }
    this.context.functions.setSort(this.props.table, {
      col: sortBy,
      dir: sortDir,
    });
    this.setState({ sortBy: sortBy, sortDir: sortDir });
  };

  clickRow = (rowNumber, data) => {
    let param = rowNumber;
    if (this.props.clickRow.param) {
      let index = this.state.colCfg.findIndex((e) => {
        return e.col === this.props.clickRow.param;
      });
      if (index < 0) {
        console.log('ERROR: ', this.props.clickRow.param + ' not found in tableconfig');
      } else {
        param = data[index];
      }
    }
    this.props.clickRow.fun(param);
  };

  makeHeader = (t) => {
    let cfg = this.state.colCfg;
    let translations = this.props.translations;

    let headerCols = [];
    let lastShownColumn = 0;
    for (let i = 0; i < cfg.length; i++) {
      if (cfg[i].show && cfg[i].width > 0) {
        lastShownColumn = i;
      }
    }
    for (let i = 0; i < cfg.length; i++) {
      if (cfg[i].show && cfg[i].width > 0) {
        let col = cfg[i].col;
        let text = '';
        let sortClass = ' ASC';
        if (this.state.sortDir < 0) {
          sortClass = ' DESC';
        }
        if (cfg[i].translate) {
          text = t(cfg[i].translate + '');
        } else {
          text = translations[cfg[i].label];
        }

        if (cfg[i].prefix) {
          text = t(cfg[i].prefix) + text;
        }

        let classes = cfg[i].className;
        if (this.state.sortBy === col) {
          classes += sortClass;
        }

        let trigger = '';
        if (i === lastShownColumn && !this.props.config) {
          trigger = (
            <Icon
              name="setting"
              style={{
                color: '#2255bb',
                position: 'absolute',
                top: '4px',
                right: '4px',
                cursor: 'pointer',
              }}
              onClick={(e) => {
                e.stopPropagation();
                if (this.props.clickSetting) {
                  this.props.clickSetting(this.props.table);
                } else {
                  this.setState({ openSetting: true });
                }
              }}
              title={t('tableConfig.title')}
            />
          );
        }

        headerCols.push(
          <Table.HeaderCell
            key={'headercol_' + i}
            onClick={(e) => {
              this.sort(col);
            }}
            className={classes}
            style={{ cursor: 'pointer' }}
            width={cfg[i].width}
          >
            {text}
            {trigger}
          </Table.HeaderCell>
        );
      }
    }
    return headerCols;
  };

  makeBody = (t, context) => {
    let cfg = this.state.colCfg;
    let data = this.props.data ? this.props.data : [];
    let sortColumn = cfg.findIndex((e) => {
      return e.col === this.state.sortBy;
    });
    if (sortColumn < 0) {
      sortColumn = 0;
    }
    let body = [];
    let list = [];
    for (let i = 0; i < data.length; i++) {
      let row = [];

      for (let k = 0; k < cfg.length; k++) {
        let val = '';
        if (cfg[k].show) {
          if (cfg[k].path) {
            let p = cfg[k].path.split('.');
            if (p.length === 1) {
              val = data[i][p[0]];
            }
            if (p.length === 2) {
              val = data[i][p[0]][p[1]];
            }
            if (cfg[k].map) {
              let found = context[cfg[k].map].find((e) => {
                return e.value === val;
              });
              if (found) {
                val = found.text;
              }
            }
            if (cfg[k].extract) {
              let extractCfg = cfg[k].extract;
              let extracted = [];
              if (!Array.isArray(val)) {
                val = [val];
              }
              for (let l = 0; l < val.length; l++) {
                if (!extractCfg.key) {
                  extracted = [];
                }
                let v = extract(val[l], extractCfg, context, data[i]);
                extracted.push(v);
              }
              val = extracted.join(cfg[k].extract.sep);
            }
            if (cfg[k].format) {
              let date = moment(val, [moment.ISO_8601, 'L']);
              if (date.isValid()) {
                val = date.format(cfg[k].format);
              } else {
                if (moment.isMoment(val)) {
                  val = val.format(cfg[k].format);
                } else {
                  //val = '';
                }
              }
            }
          }
          row.push(val);
        }
      }
      row.push({
        _trStyle: data[i]._trStyle ? data[i]._trStyle : { backgroundColor: '#ffffff' },
      });
      list.push(row);
    }

    if (cfg[sortColumn].show) {
      list.sort((a, b) => {
        let aVal = a[sortColumn] || '';
        let bVal = b[sortColumn] || '';
        if (aVal.props && aVal.props.dataval) {
          aVal = aVal.props.dataval || '';
        }
        if (bVal.props && bVal.props.dataval) {
          bVal = bVal.props.dataval || '';
        }
        if (cfg[sortColumn].format) {
          let format = cfg[sortColumn].format || 'YYYY-MM-DD HH:mm';
          aVal = moment(aVal, format);
          if (!aVal.isValid()) {
            aVal = '';
          }
          bVal = moment(bVal, format);
          if (!bVal.isValid()) {
            bVal = '';
          }
          if (aVal > bVal) return this.state.sortDir;
          if (aVal < bVal) return -this.state.sortDir;
          return 0;
        } else {
          if (
            cfg[sortColumn].cellClass === 'integer' ||
            cfg[sortColumn].cellClass === 'percent' ||
            (typeof aVal === 'number' && typeof bVal === 'number')
          ) {
            aVal = parseFloat(aVal) || 0;
            bVal = parseFloat(bVal) || 0;

            if (aVal > bVal) return this.state.sortDir;
            if (aVal < bVal) return -this.state.sortDir;
            return 0;
          } else {
            let cmp;
            if (typeof aVal === 'string' && typeof bVal === 'string') {
              try {
                cmp = aVal.localeCompare(bVal, this.props.t('language'), {
                  sensitivity: 'base',
                });
              } catch (e) {
                console.log('error', cfg[sortColumn], typeof aVal, typeof bVal, e, '\n', aVal, this.props.data);
              }
            }
            if (cmp > 0) {
              return this.state.sortDir;
            } else {
              return -this.state.sortDir;
            }
          }
        }
      });
    }
    let filtered = list;
    if (this.props.filter.length > 0) {
      let f = this.props.filter.toLowerCase();
      filtered = list.filter((e) => {
        for (let i = 0; i <= e.length; i++) {
          let v = (e[i] + '').toLowerCase();
          if (v.indexOf(f) >= 0) {
            return true;
          }
        }
        return false;
      });
    }

    for (let i = 0; i < filtered.length; i++) {
      let row = filtered[i];
      let cols = [];
      let rowStyle = { backgroundColor: '#ffffff' };
      for (let j = 0; j < row.length; j++) {
        if (row[j] && row[j]._trStyle) {
          rowStyle = row[j]._trStyle;
        }
      }
      for (let k = 0; k < cfg.length; k++) {
        if (cfg[k].show && cfg[k].width > 0) {
          let style = { position: 'relative' };
          let dStyle = {};
          let elem = row[k];
          if (elem) {
            if (elem.props && elem.props.backgroundcolor) {
              style.backgroundColor = elem.props.backgroundcolor;
              style.display = 'table-cell';
              style.textAlign = 'center';
              style.verticalAlign = 'middle';
              if (!elem.props.noshadow && elem.props.backgroundcolor !== '#ffffff') {
                style.boxShadow = 'inset 2px 2px 7px rgba(255,255,255,0.45), 2px 2px 2px rgba(0,0,0,0.35)';
              }
              dStyle = { marginLeft: '0.5rem', marginRight: '0.5rem' };
            }
            if (elem.props && elem.props.color) {
              style.color = elem.props.color;
            }
          }

          if (!row[k]) {
            row[k] = '-';
          }

          if (row[k] && !row[k]._trStyle) {
            cols.push(
              <Table.Cell key={'c_' + i + '_' + k} width={cfg[k].width} className={cfg[k].cellClass} style={style}>
                {<div style={dStyle}>{row[k]}</div>}
              </Table.Cell>
            );
          }
        }
      }
      body.push(
        <Table.Row
          style={{
            cursor: 'pointer',
            height: '1px',
            backgroundColor: rowStyle.backgroundColor,
          }}
          key={'row_' + i}
          onClick={(e) => {
            this.clickRow(i, row);
          }}
        >
          {cols}
        </Table.Row>
      );
    }
    return body;
  };

  onSubmitSetting = (colCfg) => {
    this.context.functions.setCols(this.props.table, colCfg);
    this.setState({ colCfg: colCfg, openSetting: false });
  };

  render() {
    const { t } = this.props;
    if (!this.state.colCfg) {
      return '';
    }

    let headerCols = this.makeHeader(t);
    let body = this.makeBody(t, this.context);
    return (
      <div>
        {this.props.config ? (
          ''
        ) : (
          <TableconfigModal
            open={this.state.openSetting}
            onClose={() => this.setState({ openSetting: false })}
            table={this.props.table}
            translations={this.props.translations}
            onSubmit={this.onSubmitSetting}
            trigger={''}
          />
        )}
        <Table celled style={{ marginTop: '0px', height: '1px' }}>
          <Table.Header style={{ position: 'sticky', zIndex: '99' }}>
            <Table.Row>{headerCols}</Table.Row>
          </Table.Header>
          <Table.Body className={'z95'}>{body}</Table.Body>
        </Table>
      </div>
    );
  }
}

DrTableImpl.contextType = UserContext;
const DrTable = withTranslation()(DrTableImpl);
export { DrTable };
