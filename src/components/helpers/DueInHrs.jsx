import React from 'react';
import { withTranslation } from 'react-i18next';
import moment from 'moment';
class DueInHrsImpl extends React.Component {
  render() {
    const { t } = this.props;
    let due = this.props.due;

    let time = due.due.fromNow();
    let text;

    let color = '#000000';
    let bgColor = '#ffffff';

    if (due.due < moment().add(24, 'hours')) {
      bgColor = '#ffff00';
    }

    let title = due.due.format('L HH:mm');
    if (due.shift) {
      title = due.due.format('L HH:mm') + ' - ' + due.end.format('L HH:mm');
    }

    if (due.due < moment() && due.end > moment()) {
      text = due.shift ? due.shift.description : time;
      bgColor = '#ffbb00';
    } else {
      text = due.shift ? due.shift.description + ' ' + time : time;
    }

    if (due.overdue) {
      color = '#ffffff';
      bgColor = '#cc0000';
      text = t('feedback.overdue');
      title = due.shift ? time + ' ' + due.shift.description : time;
    }

    return (
      <div style={{ lineHeight: '41px', textAlign: 'center', color: color, backgroundColor: bgColor }} title={title}>
        {text}
      </div>
    );
  }
}
const DueInHrs = withTranslation()(DueInHrsImpl);
export { DueInHrs };
