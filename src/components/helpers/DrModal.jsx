import React from 'react';
import { withTranslation } from 'react-i18next';
import { UserContext } from './../../common/UserContext.jsx';

class DrModalImpl extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      open: props.open ? props.open : false
    };
  }

  componentDidMount() {}

  componentDidUpdate(prevProps, prevState) {
    if (this.props.open !== this.state.open) {
      this.setState({ open: this.props.open });
    }
  }

  handleClose = () => {
    this.props.onClose();
    this.setState({ open: false });
  };

  render() {
    if (this.state.open) {
      return (
        <div
          style={{ position: 'fixed', height: '100vh', width: '100vw', zIndex: '2000', left: '0px', top: '0px', backgroundColor: 'rgba(0,0,0,0.8)' }}
          onClick={this.handleClose}
        >
          <div
            style={{
              position: 'fixed',
              top: '10%',
              left: '10%',
              height: '80%',
              width: '80%',
              backgroundColor: '#f0f0f0',
              borderRadius: '4px',
              border: '1px solid #aaaaaa'
            }}
            onClick={e => {
              e.stopPropagation();
            }}
          >
            {this.props.content}
          </div>
        </div>
      );
    } else {
      return this.props.trigger;
    }
  }
}

DrModalImpl.contextType = UserContext;
const DrModal = withTranslation()(DrModalImpl);
export { DrModal };
