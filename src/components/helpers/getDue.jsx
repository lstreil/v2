import moment from 'moment';

export function getDue(schedule, t) {
  let tLast = moment(schedule.lastfeedback, moment.ISO_8601);
  let setting = { list: [] };
  if (schedule.shiftsetting) {
    setting = schedule.shiftsetting.find((e) => {
      return e.language === t('language');
    });
    if (!setting) {
      setting = schedule.shiftsetting[0];
    }
  }
  setting = setting.list;
  let shifts = schedule.shift;
  let frequencydays = schedule.frequencydays;
  let duration;

  if (!shifts || !Array.isArray(shifts) || !setting || !Array.isArray(setting)) {
    let due = tLast.clone();
    due.startOf('day').add(frequencydays, 'days').add(12, 'hours');

    duration = moment.duration(due.diff(moment())).as('hours');

    return { due: due, shift: null, overdue: moment() > due, end: due, hrs: duration };
  }

  let timeLast = tLast.format('HH:mm');
  let usedShifts = [];
  for (let i = 0; i < shifts.length; i++) {
    usedShifts.push(setting[shifts[i]]);
  }

  if (usedShifts.length < 1) {
    let due = tLast.clone();
    due.startOf('day').add(frequencydays, 'days').add(12, 'hours');

    duration = moment.duration(due.diff(moment())).as('hours');
    return { due: due, shift: null, overdue: moment() > due, end: due, hrs: duration };
  }

  usedShifts.sort((a, b) => {
    if (a.from > b.from) return 1;
    if (a.from < b.from) return -1;
    return 0;
  });

  let lastShift = usedShifts.findIndex((e) => {
    if (e.from < e.to && e.from <= timeLast && e.to > timeLast) return true;
    if (e.from > e.to && (e.from <= timeLast || e.to > timeLast)) return true;
    return false;
  });

  let nextShift = (lastShift + 1) % usedShifts.length;
  let timeNext = usedShifts[nextShift].from;
  let endNext = usedShifts[nextShift].to;
  if (timeNext < timeLast) {
    // next day
    frequencydays++;
  }
  let hm = timeNext.split(':');
  let hm2 = endNext.split(':');
  let due = tLast.clone();
  let dueEnd = tLast.clone();
  due
    .startOf('day')
    .add(frequencydays - 1, 'days')
    .add(hm[0], 'hours')
    .add(hm[1], 'minutes');

  dueEnd
    .startOf('day')
    .add(frequencydays - 1, 'days')
    .add(hm2[0], 'hours')
    .add(hm2[1], 'minutes');

  if (dueEnd < due) {
    dueEnd.add(1, 'days');
  }

  duration = moment.duration(due.diff(moment())).as('hours');

  return { due: due, shift: usedShifts[nextShift], overdue: moment() > dueEnd, end: dueEnd, hrs: duration };
}
