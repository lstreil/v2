import React from 'react';
import { withTranslation } from 'react-i18next';
import { Checkbox, Dropdown } from 'semantic-ui-react';

class DrOptionImpl extends React.Component {
  toggle = (e, value, data, optiondata) => {
    let key = data.id.substr(4); // ohne 'chk_'
    let id = 'option_' + this.props.type;
    if (this.props.id) {
      id = 'option_' + this.props.id;
    }
    if (optiondata.multiple) {
      if (!Array.isArray(value)) {
        if (value) {
          value = [value];
        } else {
          value = [];
        }
      }
      let ix = value.indexOf(key + '');
      if (ix < 0) {
        value.push(key);
      } else {
        value.splice(ix, 1);
      }
    } else {
      value = key;
    }
    this.props.onChange(e, { id: id, value: value });
  };

  render() {
    const { t, cfg, type } = this.props;
    let id = 'option_' + this.props.type;
    if (this.props.id) {
      id = 'option_' + this.props.id;
    }

    if (!cfg || !cfg[type]) {
      return '';
    }

    let optiondata = cfg[type];
    if (!optiondata || !optiondata.list || (optiondata && optiondata.list && optiondata.list.length < 1)) {
      return '';
    }
    let className = optiondata && optiondata.mandatory ? 'mandatory' : '';
    let label = (
      <label htmlFor={id} className={className}>
        {t('repair.' + type)}
      </label>
    );
    let select;
    let value = this.props.value ? this.props.value : null;
    if (optiondata && optiondata.multiple) {
      if (!Array.isArray(value)) {
        if (value) {
          value = [value];
        } else {
          value = [];
        }
        if (!value) {
          value = [];
        }
      }
    } else {
      if (Array.isArray(value)) {
        value = value[0];
      }
    }

    let oList;
    if (this.props.filter) {
      let prefixes = this.props.filter;
      if (!Array.isArray(prefixes)) {
        prefixes = [prefixes];
      }
      oList = optiondata.list.filter(e => {
        for (let i = 0; i < prefixes.length; i++) {
          if (e.key.indexOf(prefixes[i]) === 0) {
            return true;
          }
        }
        return false;
      });
    } else {
      oList = optiondata.list;
    }

    if (oList.length === 0) {
      return '';
    }

    if (optiondata && optiondata.asCheckBox) {
      let checkboxes_left = [];
      let checkboxes_right = [];
      oList.sort((a, b) => {
        if (a.key + '' > b.key + '') return 1;
        if (a.key + '' < b.key + '') return -1;
        return 0;
      });
      for (let i = 0; i < oList.length; i++) {
        let entry = oList[i];
        let ix = -1;
        if (Array.isArray(value)) {
          ix = value.indexOf(entry.key);
        } else {
          if (value === entry.key) {
            ix = 0;
          }
        }
        let chk = (
          <Checkbox
            disabled={this.props.disabled}
            style={{ display: 'block' }}
            key={'chk_' + entry.key}
            name={'chk_' + type}
            id={'chk_' + entry.key}
            radio={!optiondata.multiple}
            label={entry.txt}
            checked={ix >= 0}
            onChange={(e, data) => {
              this.toggle(e, value, data, optiondata);
            }}
          />
        );
        if (i <= (oList.length - 1) / 2) {
          checkboxes_left.push(chk);
        } else {
          checkboxes_right.push(chk);
        }
      }
      select = (
        <>
          <div style={{ display: 'inline-block', width: '50%' }}>{checkboxes_left}</div>
          <div style={{ display: 'inline-block', width: '50%' }}>{checkboxes_right}</div>
        </>
      );
    } else {
      let options = [];
      let valueStyle = {};
      for (let i = 0; i < oList.length; i++) {
        let entry = oList[i];
        let style = { padding: '.78571429em 2.1em .78571429em 1em' };
        let content = <div style={style}>{entry.txt}</div>;
        if (!optiondata.multiple && oList[i].tcolor && oList[i].bcolor) {
          style.color = oList[i].tcolor;
          style.backgroundColor = oList[i].bcolor;
          content = <div style={style}>{entry.txt}</div>;
          if (oList[i].key === value) {
            valueStyle.color = oList[i].tcolor;
            valueStyle.backgroundColor = oList[i].bcolor;
          }
        }
        options.push({ key: entry.key, value: entry.key, text: entry.txt, content: content });
      }
      select = (
        <Dropdown
          disabled={this.props.disabled}
          style={valueStyle}
          className="no_padding"
          key={'option_' + type}
          id={id}
          inline
          selection
          clearable
          multiple={optiondata.multiple}
          options={options}
          value={value}
          onChange={this.props.onChange}
        />
      );
    }

    return (
      <>
        {label}
        {select}
      </>
    );
  }
}
const DrOption = withTranslation()(DrOptionImpl);

export { DrOption };
