import React from 'react';
import { withTranslation } from 'react-i18next';
import { Popup, Table } from 'semantic-ui-react';

class ShiftSettingImpl extends React.Component {
  render() {
    const { t, schedule } = this.props;
    let list = [];
    if (schedule.shiftsetting) {
      let trans = schedule.shiftsetting.find((e) => {
        return e.language === t('language');
      });
      if (!trans) {
        trans = schedule.shiftsetting[0];
      }
      list = trans.list || [];
    }
    let entry = '-';
    let content = [];
    for (let i = 0; i < list.length; i++) {
      content.push(
        <Table.Row key={'c' + i}>
          <Table.Cell style={{ fontWeight: 'bold' }}>{list[i].description.substr(0, 1)}</Table.Cell>
          <Table.Cell>{list[i].description}</Table.Cell>
          <Table.Cell>{list[i].from}</Table.Cell>
          <Table.Cell>{list[i].to}</Table.Cell>
        </Table.Row>
      );
    }
    if (content.length > 0) {
      content = (
        <Table className="small">
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell></Table.HeaderCell>
              <Table.HeaderCell>{t('forms.admin.shift.description')}</Table.HeaderCell>
              <Table.HeaderCell>{t('forms.admin.shift.from')}</Table.HeaderCell>
              <Table.HeaderCell>{t('forms.admin.shift.to')}</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>{content}</Table.Body>
        </Table>
      );
    } else {
      content = 'no shift setting';
    }
    if (schedule.shift) {
      entry = [];
      for (let i = 0; i < list.length; i++) {
        if (schedule.shift.indexOf(i) >= 0) {
          entry.push(list[i].description.substr(0, 1));
        }
      }
      entry = entry.join(', ');
    } else {
      entry = '-';
    }

    return (
      <div>
        <Popup style={{ padding: '0', margin: '0' }} content=<div>{content}</div> trigger=<div>{entry}</div> />{' '}
      </div>
    );
  }
}
const ShiftSetting = withTranslation()(ShiftSettingImpl);
export { ShiftSetting };
