import React from 'react';
import { withTranslation } from 'react-i18next';
import { TextArea, Popup, Icon } from 'semantic-ui-react';

class DrTextAreaImpl extends React.Component {
  constructor(props) {
    super(props);

    let maxLength = props.maxLength || 200;
    this.state = {
      maxLength: maxLength,
      warning: props.value.length > maxLength
    };
  }

  onChange = (e, data) => {
    if (data.value.length < this.state.maxLength) {
      this.props.onChange(e, data);
    }
    if (data.value.length > this.state.maxLength - 5) {
      this.setState({ warning: true });
    } else {
      this.setState({ warning: false });
    }
  };

  render() {
    const { t } = this.props;
    let style = {};
    let triggerstyle = { display: 'none' };
    let remaining = this.state.maxLength - this.props.value.length;
    if (remaining < 5) {
      let colors = ['#ffaa88', '#ffcc88', '#ffff88', '#ffffcc', '#ffffee'];
      style = { backgroundColor: colors[remaining], position: 'relative' };
      triggerstyle = { position: 'absolute', right: '-0.5rem', top: '-1.5rem' };
    }
    return (
      <div style={{ display: 'inline', position: 'relative' }}>
        <TextArea id="description" value={this.props.value} onChange={this.onChange} style={style} />
        <Popup
          content={t('input.maxLength') + ' - ' + t('input.remaining') + ' ' + remaining + ' ' + t('input.chars')}
          trigger={<Icon name="warning sign" color="red" style={triggerstyle} />}
        />
      </div>
    );
  }
}
const DrTextArea = withTranslation()(DrTextAreaImpl);
export { DrTextArea };
