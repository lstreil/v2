import React from "react";
export const user = {
  name: null,
  role: null
};

export const UserContext = React.createContext({ user: user });
