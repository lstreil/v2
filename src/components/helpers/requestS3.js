import axios from 'axios';
export default function putFileS3(url, file, okFun, errFun) {
  axios
    .put(url, file, {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET,PUT,POST,OPTIONS',
      },
    })
    .then(function (response) {
      okFun(response);
    })
    .catch(function (error) {
      errFun(error);
    });
}
