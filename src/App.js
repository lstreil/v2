import React from "react";
import { HashRouter as Router } from "react-router-dom";
import moment from "moment";
import "moment/locale/fr";
import "moment/locale/es";
import "moment/locale/de";
import i18n from "./i18n/i18n.jsx";
import { UserContext } from "./components/helpers/UserContext.jsx";
import request from "./components/helpers/request.js";
import { Login } from "./components/user/Login.jsx";
import { AppSelect } from "./components/AppSelect.jsx";
import "./css/style.scss";
const ls = require("local-storage");

class App extends React.PureComponent {
  constructor(props) {
    super(props);
    moment.locale(i18n.language);
    let dt = "no buildtime set";
    if (process.env.REACT_APP_BUILDDATE) {
      let tmp = process.env.REACT_APP_BUILDDATE.split(",")[0];
      dt = moment(tmp, "DD.MM.YYYY HH:mm:ss").format("YYYY-MM-DD HH:mm");
    }

    this.state = {
      buildtime: dt,
      user: null,
      roles: [],
      mounted: false
    };
  }

  componentDidMount() {
    if (!this.state.user) {
      request(
        "POST",
        "usr/ping",
        {
          info: "check if user is logged in, get accepted language otherwise"
        },
        this.pingOK,
        this.requestError
      );
    }
  }

  pingOK = (response, headers) => {
    if (!headers["accept-language"]) {
      headers["accept-language"] =
        "de-DE,de;q=0.9,fr;q=0.8,en-US;q=0.7,en;q=0.6";
    }
    let cmp =
      "," +
      headers["accept-language"].replace("-", ",") +
      " (if response has no languages we take: ,en,de,fr,... in this order";
    let de = cmp.indexOf(",de");
    let en = cmp.indexOf(",en");
    let fr = cmp.indexOf(",fr");

    let languages = [
      { key: "de", val: de },
      { key: "en", val: en },
      { key: "fr", val: fr }
    ];
    languages.sort((a, b) => {
      if (a.val < b.val) return -1;
      if (a.val > b.val) return 1;
      return 0;
    });

    let language = languages[0].key;
    i18n.changeLanguage(language, () => {
      moment.locale(language);
    });

    if (response === "no users" || response === "bootstrap ok") {
      this.setState({ language: language, bootstrap: true, mounted: true });
    } else {
      if (response.user && response.user.d_uuid) {
        this.setUser(response.user);
      } else {
        this.setState({
          language: language,
          mounted: true
        });
      }
    }
  };

  requestError = error => {
    console.log("requestError", error);
    if (error.status === 401 || error.status === 503) {
      this.setState({ user: null });
    } else {
      this.setState({
        error: { status: error.status, response: error.data },
        timer: setTimeout(this.clearError, 15000),
        mounted: true,
        loading: false
      });
    }
  };

  setUser = user => {
    console.log("setUser", user);
    ls.set("DR-session", user._sessionid);
    //UserContext = React.createContext(user);
    this.setState({ user: user, roles: user.customerroles, mounted: true });
  };

  render() {
    if (this.state.mounted) {
      if (!this.state.user) {
        return (
          <Router>
            <Login
              key="login"
              setUser={this.setUser}
              text={""}
              build={this.state.buildtime}
            />
          </Router>
        );
      } else {
        return (
          <Router>
            <UserContext.Provider
              value={{
                build: this.state.build,
                user: this.state.user,
                logout: this.logout,
                roles: this.state.roles,
                rights: this.state.rights,
                functions: this.state.functions,
                tableconfigs: this.state.tableconfigs
              }}
            >
              <AppSelect />
            </UserContext.Provider>
          </Router>
        );
      }
    } else {
      return "not yet mounted";
    }
  }
}

export default App;
